package com.pietro.materialdesign;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.pietro.materialdesigncard.R;


/**
 * Created by pietrogirardi on 7/10/15.
 */
public class MyToolbar {

    private Toolbar mToolbar;
    private Toolbar mToolbarBottom;
    private Activity activity;

    public MyToolbar(Activity activity) {
        this.activity = activity;
    }

    public void addDefaultToolbar() {
        addToolbar(activity.getString(R.string.main_title), activity.getString(R.string.sub_title));
    }

    public void addToolbar() {
        addToolbar(null, null);
    }

    public void addToolbar(String title, String subTitle) {

            mToolbar = (Toolbar) activity.findViewById(R.id.inc_toolbar);

        if(null != title)
            mToolbar.setTitle(title);

        if(null != subTitle)
            mToolbar.setSubtitle(subTitle);

            mToolbar.setLogo(R.mipmap.ic_launcher);
            ((ActionBarActivity)activity).setSupportActionBar(mToolbar);

            mToolbarBottom = (Toolbar) activity.findViewById(R.id.inc_tb_bottom);
            mToolbarBottom.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    Intent intent = null;

                    switch (menuItem.getItemId()) {
                        case R.id.action_facebook:
                            intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(activity.getString(R.string.link_facebook)));
                            break;

                        case R.id.action_youtube:
                            intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(activity.getString(R.string.link_youtube)));
                            break;

                        case R.id.action_google_plus:
                            intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(activity.getString(R.string.link_google_plus)));
                            break;

                        case R.id.action_linkedin:
                            intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(activity.getString(R.string.link_linkedin)));
                            break;

                        case R.id.action_whatsapp:
                            intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(activity.getString(R.string.link_whatsapp)));
                            break;

                    }
                    activity.startActivity(intent);
                    return true;
                }
            });
            mToolbarBottom.inflateMenu(R.menu.menu_bottom);
            mToolbarBottom.findViewById(R.id.btn_settings).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, activity.getString(R.string.action_settings), Toast.LENGTH_LONG).show();
                }
            });
    }

}
