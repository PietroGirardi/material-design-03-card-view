package com.pietro.materialdesign.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pietro.materialdesign.DAO.Car;
import com.pietro.materialdesign.MainActivity;
import com.pietro.materialdesign.adapters.CarAdapter;
import com.pietro.materialdesign.interfaces.RecyclerViewOnClickListenerHack;
import com.pietro.materialdesigncard.R;

import java.util.List;

/**
 * Created by pietrogirardi on 7/25/15.
 */
public class CarFragment extends Fragment implements RecyclerViewOnClickListenerHack{

    RecyclerView rvCars;
    List<Car> listCar;
    CarAdapter carAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_car, container, false);

        rvCars = (RecyclerView) view.findViewById(R.id.rv_list);
        rvCars.setHasFixedSize(true);

        rvCars.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager llm = (LinearLayoutManager) rvCars.getLayoutManager();
                CarAdapter carAdapter = (CarAdapter) rvCars.getAdapter();
                if(listCar.size() == llm.findLastCompletelyVisibleItemPosition()+1){
                    List<Car> listCarAux = ((MainActivity) getActivity()).getCarList(10);
                    for(int i=0;i<listCarAux.size();i++){
                        carAdapter.addCar(listCarAux.get(i),listCar.size());
                    }
                }
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvCars.setLayoutManager(llm);

        listCar = ((MainActivity) getActivity()).getCarList(10);
        carAdapter = new CarAdapter(getActivity(), listCar);
        carAdapter.setRecyclerViewOnClickListenerHack(this);
        rvCars.setAdapter(carAdapter);
        return view;
    }

    @Override
    public void onClickListener(View view, int position) {
        Toast.makeText(getActivity(), "position: "+position, Toast.LENGTH_SHORT).show();
        carAdapter.removeCar(position);
    }
}