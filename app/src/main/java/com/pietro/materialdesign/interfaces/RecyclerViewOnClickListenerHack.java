package com.pietro.materialdesign.interfaces;

import android.view.View;

/**
 * Created by pietrogirardi on 7/25/15.
 */
public interface RecyclerViewOnClickListenerHack {
    public void onClickListener(View view, int position);
}
